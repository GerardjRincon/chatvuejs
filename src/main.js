import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import vueCookies from 'vue-cookies'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'



Vue.config.productionTip = false


router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.autorizado)) {
     if (!store.getters.EndSession) {
       next({
         name: 'login'
       })
     } else {
       next()
     }
  } else if (to.matched.some(record => record.meta.visita)) {
   if (store.getters.EndSession) {
     next({
       name: 'chat'
     })
   } else {
     next()
   }
} else {
 next()
}
})


new Vue({
    router,
    store,
    vueCookies,
    render: h => h(App)
  }).$mount('#app')
