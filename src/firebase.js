import firebase from '@firebase/app'

require('firebase/auth')
require('firebase/firestore')

var firebaseConfig = {
    apiKey: "AIzaSyB8ilMce3UVLPJ7A6qNKdO2BY_JrjDv4aY",
    authDomain: "chat-a4458.firebaseapp.com",
    databaseURL: "https://chat-a4458.firebaseio.com",
    projectId: "chat-a4458",
    storageBucket: "chat-a4458.appspot.com",
    messagingSenderId: "842648195191",
    appId: "1:842648195191:web:593a27e65c18dc05"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  
  const auth = firebase.auth();
  const db = firebase.firestore();
  const signOut = firebase.auth().signOut();

  export {auth, db, signOut}