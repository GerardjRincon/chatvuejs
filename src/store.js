import Vue from 'vue'
import Vuex from 'vuex'
import {auth, db} from '@/firebase'
import vueCookies from 'vue-cookies'
import { storage } from 'firebase';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: window.$cookies.get('user_token') || null
  },
  getters: {
    EndSession(state){
      return state.user !== null
    }

  },
  mutations: {
    Login(state, user) {
      state.user = user
    },
    Logout(state) {
      state.user = null
    }
  },
  actions: {
    async Login(context, credenciales) {
    
      await auth.signInWithEmailAndPassword(credenciales.email, credenciales.password)

      if (auth.currentUser) {
        const user = auth.currentUser.uid
        let doc = await db.collection('usuario')
            .doc(user)
            .get()
            
        if (doc.exists) {
            let usuario = doc.data()
            let batch = db.batch()
            batch.update(
                db.collection('usuario')
                .doc(usuario.uid),
                { Status: true }
            )
      batch.commit()
            vueCookies.set('user_token',  usuario.uid) 
            // localStorage.setItem('user_token', usuario.uid)
            context.commit('Login', usuario.uid)
            
        }
      }
  },
  Logout(context,us) {
    if (context.getters.EndSession) {
      // localStorage.removeItem('user_token')
      let batch = db.batch()
        batch.update(
            db.collection('usuario')
            .doc(us),
            { Status: false }
        )
      batch.commit()

      vueCookies.remove('user_token')
      context.commit('Logout')

    }
  }
}
})
